import click
from typedb.client import SessionType, TransactionType, TypeDB
from typedb.common.exception import TypeDBClientException

from .io import debug, fprint, info, page_markdown, perror
from .text import extract_title


def define_schema(database):
    schema = """
    define
        zettel sub entity,
            owns title @key,
            owns content,
            plays link:node;
        title sub attribute,
            value string;
        content sub attribute,
            value string;
        link sub relation,
            relates node;
    """
    with TypeDB.core_client("localhost:1729") as client:
        with client.session(database, SessionType.SCHEMA) as session:
            with session.transaction(TransactionType.WRITE) as transaction:
                transaction.query().define(schema)
                transaction.commit()


@click.group()
@click.pass_context
def cli(ctx):
    ctx.ensure_object(dict)


@cli.group()
def vault():
    pass


@vault.command(name="list")
def vault_list():
    info("List of vaults.")
    with TypeDB.core_client("localhost:1729") as client:
        try:
            for idx, db in enumerate(client.databases().all(), 1):
                db = db.name()
                if db.endswith("-vault"):
                    fprint(f"{idx:>4}: {db.removesuffix('-vault')}")
        except TypeDBClientException as exc:
            perror(f"{exc.message}")


@vault.command()
@click.argument("database")
@click.pass_context
def use(ctx, database):
    info(f"Using {database}")
    with TypeDB.core_client("localhost:1729") as client:
        if client.databases().contains(f"{database}-vault"):
            ctx.obj["database"] = database
        else:
            perror(f"Vault {database} does not exist")


@vault.command()
@click.argument("database")
@click.pass_context
def create(ctx, database):
    info(f"Creating {database}")
    ctx.obj["database"] = database
    database = f"{database}-vault"
    with TypeDB.core_client("localhost:1729") as client:
        try:
            client.databases().create(database)
            define_schema(database)
            fprint(f"{database=}")
        except TypeDBClientException as exc:
            perror(f"{exc.message}")


@vault.command()
@click.argument("database")
@click.pass_context
def delete(ctx, database):
    info(f"Deleting {database}")
    ctx.obj["database"] = ""
    with TypeDB.core_client("localhost:1729") as client:
        try:
            client.databases().get(f"{database}-vault").delete()
        except TypeDBClientException as exc:
            perror(f"{exc.message}")


@cli.command()
@click.pass_context
def list_notes(ctx):
    database = ctx.obj["database"]
    if not database:
        perror("No database defined")
        return
    query = """
        match $n isa zettel,
        has title $title;
        get $title;
    """
    with TypeDB.core_client("localhost:1729") as client:
        with client.session(f"{database}-vault", SessionType.DATA) as session:
            with session.transaction(TransactionType.READ) as transaction:
                matches = transaction.query().match(query)
                titles = sorted(ans.get("title").get_value() for ans in matches)
                page_markdown("\n".join(titles), "Titles")


def read_note(title, database):
    query = f"""
        match $n isa zettel,
        has title '{title}',
        has content $content;
        get $content;
    """
    with TypeDB.core_client("localhost:1729") as client:
        with client.session(f"{database}-vault", SessionType.DATA) as session:
            with session.transaction(TransactionType.READ) as transaction:
                matches = transaction.query().match(query)
                notes = [ans.get("content") for ans in matches]
                assert len(notes) <= 1
                return notes[0].get_value() if notes else None


@cli.command()
@click.argument("title", required=True)
@click.pass_context
def get_note(ctx, title):
    database = ctx.obj["database"]
    if not database:
        perror("No database defined")
        return
    if note := read_note(title, database):
        page_markdown(note, title)


@cli.command()
@click.argument("title", default="")
@click.pass_context
def write_note(ctx, title):
    database = ctx.obj["database"]
    if not database:
        perror("No database defined")
        return
    debug(f"{title}=")
    note = read_note(title, database)
    note = click.edit(f"# {title}\n" if note is None else note)
    if note is None:
        return
    title = extract_title(note)
    if not title:
        perror("No valid title")
        return
    ##TODO: links = []
    qdelete = f"""
        match $n isa zettel,
        has title '{title}';
        delete $n isa zettel;
    """
    qinsert = f"""
        insert $n isa zettel,
        has title '{title}',
        has content '{note}';
    """
    with TypeDB.core_client("localhost:1729") as client:
        with client.session(f"{database}-vault", SessionType.DATA) as session:
            with session.transaction(TransactionType.WRITE) as transaction:
                transaction.query().delete(qdelete)
                transaction.query().insert(qinsert)
                try:
                    transaction.commit()
                except TypeDBClientException as exc:
                    perror(f"{exc.message}")
