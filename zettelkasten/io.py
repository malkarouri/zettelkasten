# io.py

__all__ = ["fprint", "debug", "perror", "page_markdown", "prompt", "info"]

from pathlib import Path
from subprocess import run

from prompt_toolkit import HTML, PromptSession
from prompt_toolkit import print_formatted_text as fprint
from prompt_toolkit.history import FileHistory

DEBUG = True


def debug(message):
    if DEBUG:
        fprint(HTML(f"<skyblue>DEBUG:</skyblue> {message}"))


def info(message):
    fprint(HTML(f"<ansiyellow>{message}</ansiyellow>"))


def page_markdown(text, title=""):
    return run(
        ["bat", "--language=markdown", f"--file-name={title}"],
        input=text,
        encoding="utf-8",
        check=True,
    )


def perror(message):
    fprint(HTML(f"<orangered>{message}</orangered>"))


class Prompt:
    def __init__(self) -> None:
        self.session = PromptSession(
            history=FileHistory(Path("~/.zettelhistory").expanduser())
        )

    def __call__(self, message: str) -> str:
        return self.session.prompt(HTML(f"<skyblue>{message}>></skyblue> "))


prompt = Prompt()
