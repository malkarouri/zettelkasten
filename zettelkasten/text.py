# text.py

__all__ = ["extract_title"]

from marko import Markdown, ast_renderer, inline


class WikiLinkElement(inline.InlineElement):
    pattern = r"\[\[(?:[^|\]]*\|)?([^\]]+)\]\]"
    parse_children = True

    def __init__(self, match):
        self.target = match.group(1)


class WikiRendererMixin:
    def render_github_wiki(self, element):
        return '<a href="{}">{}</a>'.format(
            self.escape_url(element.target), self.render_children(element)
        )


class WikiLink:
    elements = [WikiLinkElement]
    renderer_mixins = [WikiRendererMixin]


def _get_title(doc):
    if doc.get("level") == 1:
        return [doc.get("children")]
    if isinstance(doc.get("children"), list):
        return sum((_get_title(d) for d in doc.get("children")), start=[])
    return []


def extract_title(note):
    markdown = Markdown(renderer=ast_renderer.ASTRenderer, extensions=[WikiLink])
    parsed = markdown.convert(note)
    titled = _get_title(parsed)
    return titled[0][0]["children"] if titled else ""
