#!/usr/bin/env python

from .commands import cli
from .io import debug, perror, prompt


def main():
    context = {"database": ""}
    while True:
        try:
            answer = prompt(context["database"])
            answer = answer.strip()
        except KeyboardInterrupt:
            continue
        except EOFError:
            break
        else:
            if answer == "quit":
                break
            debug(answer)
            try:
                debug(f"{context=}")
                cli.main(answer.split(), obj=context, standalone_mode=False)
            except Exception as exc:
                perror(exc.message)


if __name__ == "__main__":
    main()
