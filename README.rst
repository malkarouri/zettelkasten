============
Zettelkasten
============

This is a project for building and conversing with a Zettelkasten.

It works from the command line and interacts with a TypeDB database.

Commands
--------

1. Vault
2. Article Editing
3. Graph
4. Outline
